
import React, { PureComponent } from "react";
import {
  Animated,
  AppRegistry,
  StyleSheet,
  Dimensions,
  View,
  Text,
  StatusBar,
  TouchableHighlight,
  Image,
  ImageBackground
} from "react-native";
import { GameLoop } from "react-native-game-engine";

const RADIUS = 25;

const levelTable = {
  1: { score: 95, speed: 2000 },
  2: { score: 195, speed: 1000 },
  3: { score: 295, speed: 800 },
  4: { score: 395, speed: 700 },
  5: { score: 495, speed: 600 },
  6: { score: 595, speed: 500 },
  7: { score: 695, speed: 400 },
  8: { score: 795, speed: 300 },
  9: { score: 895, speed: 200 },
}
const imagesNextLevelTwo = [
  require('./src/img/ib_san_anim_1.png'),
  require('./src/img/ib_san_anim_2.png'),
  require('./src/img/ib_san_anim_3.png')
];
const imagesNextLevelThree = [
  require('./src/img/ib_san_anim_lvl2_0.png'),
  require('./src/img/ib_san_anim_lvl2_0_a.png'),
  require('./src/img/ib_san_2.png'),
];
const imagesNextLevelFour = [
  require('./src/img/ib_san_anim_lvl2_1.png'),
  require('./src/img/ib_san_anim_lvl2_2.png'),
  require('./src/img/ib_san_anim_lvl2_3.png')
];
const imagesNextLevelFive = [
  require('./src/img/ib_san_2.png'),
  require('./src/img/ib_san_3_0.png'),
  require('./src/img/ib_san_3.png')
];
const imagesNextLevelSixToEnd = [
  require('./src/img/ib_san_anim_lvl3_1.png'),
  require('./src/img/ib_san_anim_lvl3_2.png'),
  require('./src/img/ib_san_anim_lvl3_3.png')
];
const imagesGameOver = [
  require('./src/img/ib_san_game_over_1.png'),
  require('./src/img/ib_san_game_over_2.png'),
  require('./src/img/ib_san_game_over.png')
];
export default class IbanCrush extends PureComponent {
  constructor() {
    super();
    this.state = {
      firstStart: true,
      x: Dimensions.get("window").width / 2 - 25,
      y: Dimensions.get("window").height / 2 - 50,
      points: 0,
      gameOver: false,
      level: 1,
      timeCounter: null,
      touched: false,
      timeout: false,
      touchedCounter: 0,
      imageIndex: 0,
      intervalId: null,
      fadeAnimTarget: new Animated.Value(1),
      fadeAnimTargetLevelPass: new Animated.Value(0),
      fadeAnimLevel: new Animated.Value(0),
      fadeAnimPoints: new Animated.Value(0),
      fadeAnimText: new Animated.Value(0),
      fadeAnimButton: new Animated.Value(0)
    };
    // Event Listener for orientation changes
    Dimensions.addEventListener('change', () => {
      this.setState({
        x: Dimensions.get("window").width / 2 - 25,
        y: Dimensions.get("window").height / 2 - 50
      });
    });
  }
  isPortrait = () => {
    const dim = Dimensions.get('window');
    return dim.height >= dim.width;
  };
  updateHandler = ({ touches, screen, layout, time }) => {
    let touch = touches.find(x => x.type === "press");
    // if target is not touched in time
    if (this.state.timeCounter && time.previous && this.state.timeCounter + levelTable[this.state.level].speed <= time.previous) {
      this.setState({ timeCounter: null, gameOver: true, timeout: true });

      Animated.timing(this.state.fadeAnimText, {
        toValue: 1,
        duration: 1000,
        useNativeDriver: false
      }).start(() => Animated.timing(this.state.fadeAnimLevel, {
        toValue: 1,
        duration: 1000,
        useNativeDriver: false
      }).start(() => Animated.timing(this.state.fadeAnimPoints, {
        toValue: 1,
        duration: 1000,
        useNativeDriver: false
      }).start(() =>
        Animated.timing(this.state.fadeAnimTargetLevelPass, {
          toValue: 1,
          duration: 1000,
          useNativeDriver: false
        }).start(() => {
          this.renderImageGameOver();
          Animated.timing(this.state.fadeAnimButton, {
            toValue: 1,
            duration: 100,
            useNativeDriver: false
          }).start()
        }
        ))));
    }
    if (touch) {
      const randomNumberX = Math.floor(Math.random() * Math.floor(Dimensions.get("window").width - 50));
      const randomNumberY = Math.floor(Math.random() * Math.floor(Dimensions.get("window").height - 80));
      const randomExcludeZeroX = randomNumberX < 10 ? 10 : randomNumberX;
      const randomExcludeZeroY = randomNumberY < 10 ? 10 : randomNumberY;

      // if target is touched in time
      if ((this.state.x <= touch.event.pageX && touch.event.pageX < this.state.x + 100)
        && this.state.y <= touch.event.pageY && touch.event.pageY < this.state.y + 100) {
        this.setState({
          firstStart: false,
          touched: true,
          timeCounter: Date.now(),
          points: this.state.points + 5,
          touchedCounter: this.state.touchedCounter + 1
        }, () => {
          // if we level up
          if (this.state.points > levelTable[this.state.level].score && this.state.level < 9) {
            this.setState({ level: this.state.level + 1, changeLevel: true, timeCounter: null });

            Animated.timing(this.state.fadeAnimLevel, {
              toValue: 1,
              duration: 1000,
              useNativeDriver: false
            }).start(() =>
              Animated.timing(this.state.fadeAnimPoints, {
                toValue: 1,
                duration: 1000,
                useNativeDriver: false
              }).start(() => {
                Animated.timing(this.state.fadeAnimText, {
                  toValue: 1,
                  duration: 1000,
                  useNativeDriver: false
                }).start(() => {
                  Animated.timing(this.state.fadeAnimTargetLevelPass, {
                    toValue: 1,
                    duration: 1000,
                    useNativeDriver: false
                  }).start(() => {
                    this.renderImageNextLevel();
                    Animated.timing(this.state.fadeAnimButton, {
                      toValue: 1,
                      duration: 100,
                      useNativeDriver: false
                    }).start()
                  })
                });
              }));
          }
        });

        // Handle target opacity animation according to level speed
        this.state.fadeAnimTarget.setValue(1);
        Animated.timing(this.state.fadeAnimTarget, {
          toValue: 0,
          duration: levelTable[this.state.level].speed,
          useNativeDriver: false
        }).start();
      }
      else {
        // if we did not touch the target => game over
        this.setState({
          touched: false,
          gameOver: true
        });
        Animated.timing(this.state.fadeAnimText, {
          toValue: 1,
          duration: 1000,
          useNativeDriver: false
        }).start(() =>
          Animated.timing(this.state.fadeAnimLevel, {
            toValue: 1,
            duration: 1000,
            useNativeDriver: false
          }).start(() => Animated.timing(this.state.fadeAnimPoints, {
            toValue: 1,
            duration: 1000,
            useNativeDriver: false
          }).start(() => Animated.timing(this.state.fadeAnimTargetLevelPass, {
            toValue: 1,
            duration: 1000,
            useNativeDriver: false
          }).start(() => {
            this.renderImageGameOver();
            Animated.timing(this.state.fadeAnimButton, {
              toValue: 1,
              duration: 100,
              useNativeDriver: false
            }).start()
          })))
        )
      }
      this.setState({
        x: randomExcludeZeroX,
        y: randomExcludeZeroY
      });
    }
  };
  restart() {
    // clean animation
    clearInterval(this.state.intervalId);
    Animated.timing(this.state.fadeAnimTargetLevelPass).stop();
    Animated.timing(this.state.fadeAnimLevel).stop();
    Animated.timing(this.state.fadeAnimPoints).stop();
    Animated.timing(this.state.fadeAnimText).stop();
    Animated.timing(this.state.fadeAnimButton).stop();
    this.setState({
      firstStart: true,
      touched: false,
      gameOver: false,
      imageIndex: 0,
      x: Dimensions.get("window").width / 2 - 25,
      y: Dimensions.get("window").height / 2 - 50,
      points: 0,
      level: 1,
      timeCounter: null,
      intervalId: null,
      fadeAnimTarget: new Animated.Value(1),
      fadeAnimTargetLevelPass: new Animated.Value(0),
      fadeAnimLevel: new Animated.Value(0),
      fadeAnimPoints: new Animated.Value(0),
      fadeAnimText: new Animated.Value(0),
      fadeAnimButton: new Animated.Value(0)
    })
  }
  continueNextLevel() {
    // clean animation
    clearInterval(this.state.intervalId);
    Animated.timing(this.state.fadeAnimTargetLevelPass).stop();
    Animated.timing(this.state.fadeAnimLevel).stop();
    Animated.timing(this.state.fadeAnimPoints).stop();
    Animated.timing(this.state.fadeAnimText).stop();
    Animated.timing(this.state.fadeAnimButton).stop();
    this.setState({
      ...this.state,
      changeLevel: false,
      imageIndex: 0,
      intervalId: null,
      x: Dimensions.get("window").width / 2 - 25,
      y: Dimensions.get("window").height / 2 - 50,
      fadeAnimTarget: new Animated.Value(1),
      fadeAnimTargetLevelPass: new Animated.Value(0),
      fadeAnimLevel: new Animated.Value(0),
      fadeAnimPoints: new Animated.Value(0),
      fadeAnimText: new Animated.Value(0),
      fadeAnimButton: new Animated.Value(0)
    })
  }
  renderImageNextLevel() {
    let intervalId = setInterval(() => {
      var imageIndex = this.state.imageIndex + 1;
      if ((this.state.level === 2 && imageIndex >= imagesNextLevelTwo.length)
        || (this.state.level === 4 && imageIndex >= imagesNextLevelFour.length)
        || (this.state.level > 5 && imageIndex >= imagesNextLevelSixToEnd.length)) {
        imageIndex = 0;
      }
      if ((this.state.level === 3 && imageIndex >= imagesNextLevelThree.length)
        || (this.state.level === 5 && imageIndex >= imagesNextLevelFive.length)) {
        imageIndex = this.state.level === 3 ? imagesNextLevelThree.length - 1 : imagesNextLevelFive.length - 1;
        clearInterval(this.state.intervalId);
      }
      this.setState({ imageIndex: imageIndex })
    }, 500);
    this.setState({ intervalId: intervalId })
  }
  renderImageGameOver() {
    let intervalId = setInterval(() => {
      var imageIndex = this.state.imageIndex + 1;
      if (imageIndex >= imagesGameOver.length) {
        imageIndex = imagesGameOver.length - 1;
        clearInterval(this.state.intervalId);
      }
      this.setState({ imageIndex: imageIndex })
    }, 500);
    this.setState({ intervalId: intervalId })
  }
  render() {
    return (
      this.state.gameOver ?
        <View style={{ flex: 1, justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}>
          <ImageBackground source={require('./src/img/ib_san_bckg_game_over.png')} style={styles.imageBackground}>
            <View style={{ flex: 1, justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}>
              <Animated.View style={{ flex: 1, marginTop: this.isPortrait() ? 20 : 5, opacity: this.state.fadeAnimLevel }}>
                <Text style={{ flex: 1, fontSize: 30, fontWeight: "bold" }}>
                  {`Niveau ${this.state.level}`}
                </Text>
              </Animated.View>
              <Animated.View style={{ flex: 1, opacity: this.state.fadeAnimPoints }}>
                <Text style={{
                  flex: 1,
                  fontSize: 30,
                  fontWeight: "bold",
                }}>
                  {`${this.state.points} points`}
                </Text>
              </Animated.View>

              <Animated.View style={{
                flex: 1,
                justifyContent: 'flex-start',
                alignItems: 'center',
                opacity: this.state.fadeAnimText
              }}>
                <Text style={{ fontSize: 30, fontStyle: "italic" }}>
                  {`${!this.state.touched ? 'Cible manquée !' : this.state.timeout ? 'Pas assez rapide !' : ''}`}
                </Text>
              </Animated.View>
              <Animated.View
                style={
                  {
                    opacity: this.state.fadeAnimTargetLevelPass,
                    width: RADIUS * 2,
                    height: RADIUS * 2,
                    marginRight: 50,
                    marginBottom: !this.isPortrait() ? 20 : 0
                  }
                }
              >
                <Image source={imagesGameOver[this.state.imageIndex]} style={{ margin: 10 }} />
              </Animated.View>
            </View>
            <Animated.View style={{
              flex: 1,
              justifyContent: 'flex-start',
              alignItems: 'center',
              opacity: this.state.fadeAnimButton,
            }}>
              <TouchableHighlight
                style={{ ...styles.submit, margin: this.isPortrait() ? styles.submit.margin : 10 }}
                onPress={() => this.restart()}
                disabled={!this.state.intervalId}>
                <Text style={{ ...styles.submitText }}>NOUVEL ESSAI</Text>
              </TouchableHighlight>
            </Animated.View>
            <StatusBar hidden={true} />
          </ImageBackground>
        </View>
        :
        this.state.changeLevel ?
          <View style={{ flex: 1, justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}>
            <ImageBackground source={require('./src/img/ib_san_bckg_game_over.png')} style={styles.imageBackground}>
              <View style={{ flex: 1, justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}>
                <Animated.View style={{ flex: 1, marginTop: this.isPortrait() ? 20 : 5, opacity: this.state.fadeAnimLevel }}>
                  <Text style={{ flex: 1, fontSize: 30, fontWeight: "bold" }}>
                    {`Niveau ${this.state.level}`}
                  </Text>
                </Animated.View>
                <Animated.View style={{ flex: 1, opacity: this.state.fadeAnimPoints }}>
                  <Text style={{
                    flex: 1,
                    fontSize: 30,
                    fontWeight: "bold",
                  }}>
                    {`${this.state.points} points`}
                  </Text>
                </Animated.View>
                <Animated.View style={{ flex: 1, opacity: this.state.fadeAnimText }}>
                  <Text style={{
                    flex: 1,
                    fontSize: 30,
                    marginHorizontal: 10,
                    fontStyle: 'italic',
                    textAlign: 'center'
                  }}>
                    {`Goku disparaît au bout de : ${levelTable[this.state.level].speed / 1000} seconde`}
                  </Text>
                </Animated.View>
                <Animated.View
                  style={
                    {
                      opacity: this.state.fadeAnimTargetLevelPass,
                      width: RADIUS * 2,
                      height: RADIUS * 2,
                      marginBottom: this.state.level === 3 || this.state.level === 5 ? 20 : 0
                    }
                  }
                >
                  <Image
                    source={this.state.level === 2 ?
                      imagesNextLevelTwo[this.state.imageIndex]
                      : this.state.level === 3 ?
                        imagesNextLevelThree[this.state.imageIndex]
                        : this.state.level === 4 ?
                          imagesNextLevelFour[this.state.imageIndex]
                          : this.state.level === 5 ?
                            imagesNextLevelFive[this.state.imageIndex]
                            : imagesNextLevelSixToEnd[this.state.imageIndex]
                    } />
                </Animated.View>
              </View>
              <Animated.View style={{
                flex: 1,
                justifyContent: 'flex-start',
                alignItems: 'center',
                opacity: this.state.fadeAnimButton,
              }}>
                <TouchableHighlight
                  style={{ ...styles.submit, margin: this.isPortrait() ? styles.submit.margin : 30 }}
                  onPress={() => this.continueNextLevel()}
                  disabled={!this.state.intervalId}>
                  <Text style={{ ...styles.submitText }}>C'est parti !</Text>
                </TouchableHighlight>
              </Animated.View>
              <StatusBar hidden={true} />
            </ImageBackground>
          </View>
          :
          <GameLoop
            style={styles.container}
            onUpdate={this.updateHandler}
          >
            {
              this.state.firstStart ?
                <View
                  style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignContent: 'center',
                    alignItems: 'center'
                  }}
                >
                  <ImageBackground source={require('./src/img/ib_san_land_1.jpg')} style={styles.imageBackground}>
                    <Text style={{
                      flex: 1,
                      fontSize: 30,
                      fontWeight: "bold",
                      textAlign: 'center'
                    }}>{'Appuies sur Goku pour commencer'}</Text>
                  </ImageBackground>
                </View>
                : <ImageBackground source={
                  this.state.level === 1 ?
                    require('./src/img/ib_san_land_1.jpg')
                    : this.state.level === 2 ?
                      require('./src/img/ib_san_land_3.jpg')
                      : this.state.level === 3 ?
                        require('./src/img/ib_san_land_2.jpg')
                        : this.state.level === 4 ?
                          require('./src/img/ib_san_land_1.jpg')
                          : this.state.level === 5 ?
                            require('./src/img/ib_san_land_3.jpg')
                            : this.state.level === 6 ?
                              require('./src/img/ib_san_land_2.jpg')
                              : this.state.level === 7 ?
                                require('./src/img/ib_san_land_1.jpg')
                                : this.state.level === 8 ?
                                  require('./src/img/ib_san_land_3.jpg')
                                  : require('./src/img/ib_san_land_2.jpg')
                }
                  style={styles.imageBackground} />}
            <Animated.View
              style={[
                styles.target,
                {
                  opacity: this.state.fadeAnimTarget,
                  left: this.state.x, top: this.state.y
                }
              ]}
            >

              <Image source={
                this.state.level <= 2 ?
                  require('./src/img/ib_san_1_a.png')
                  : this.state.level <= 4 ?
                    require('./src/img/ib_san_2_a.png')
                    : require('./src/img/ib_san_3.png')
              } />
            </Animated.View>
            <StatusBar hidden={true} />
          </GameLoop>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "black",
    padding: 10,
    paddingBottom: 15
  },
  target: {
    position: "absolute",
    width: RADIUS * 2,
    height: RADIUS * 2
  },
  submit: {
    width: 120,
    height: 120,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 50,
    padding: 10,
    borderRadius: 100,
    backgroundColor: '#131eeb',
    elevation: 5
  },
  submitText: {
    color: '#fff',
    textAlign: 'center',
  },
  imageBackground: {
    width: '100%',
    height: '100%',
    resizeMode: 'cover',
    justifyContent: "center"
  }
});

AppRegistry.registerComponent("BestGameEver", () => IbanCrush);